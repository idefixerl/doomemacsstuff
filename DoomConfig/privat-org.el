;;; privat-org.el -*- lexical-binding: t; -*-

(setq org-directory "~/org/privat/")
(setq org-roam-directory "~/org/privat/roam/")
(setq org-agenda-files '("~/org/privat"))

(setq org-capture-templates
      '(("t" "Todo" entry (file+headline "~/org/privat/todo.org" "Tasks")
         "* TODO %?\n  %u\n  %a")
        ("n" "Note" entry (file+headline "~/org/privat/notes.org" "Notes")
         "* %?\n  %u\n  %a")))
