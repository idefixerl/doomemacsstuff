;;; arbeit-org.el -*- lexical-binding: t; -*-

(setq org-directory "~/org/arbeit/")
(setq org-roam-directory "~/org/arbeit/roam/")
(setq org-agenda-files '("~/org/arbeit"))

(setq org-capture-templates
      '(("t" "Todo" entry (file+headline "~/org/arbeit/todo.org" "Tasks")
         "* TODO %?\n  %u\n  %a")
        ("n" "Note" entry (file+headline "~/org/arbeit/notes.org" "Notes")
         "* %?\n  %u\n  %a")))
